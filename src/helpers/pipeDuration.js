//Formats a number into a number of times according to the given template (hh:mm hours)
export const toCurrentDuration = (num) => {
	let hours = Math.floor(num / 60).toString();
	let minutes = (num - hours * 60).toString();
	hours = hours.length === 0 ? '00' : hours.length === 1 ? '0' + hours : hours;
	minutes =
		minutes.length === 0
			? '00'
			: minutes.length === 1
			? '0' + minutes
			: minutes;
	return `${hours}:${minutes} hours`;
};

//By id, searches for the names of authors in an array of objects and returns
// their names separated by commas
export const toAuthorsString = (authors, authorsList) => {
	const authorList = authors.map((item) => {
		return authorsList.filter((elem) => elem.id === item)[0];
	});
	const names = authorList.map((item) => item.name).join(', ');
	return names.length < 35 ? names : names.slice(0, 33) + '...';
};
