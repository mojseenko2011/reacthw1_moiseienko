import logo from '../../../../pictures/coursesLogo.png';

const Logo = () => {
	return (
		<img src={logo} style={{ width: '100%' }} alt='Main logo for this page' />
	);
};

export default Logo;
