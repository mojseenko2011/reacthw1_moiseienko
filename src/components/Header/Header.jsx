import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';
import { BUTTON_TEXT_LOGOUT } from '../../constants';

import './header.scss';

const Header = () => {
	return (
		<div className='header'>
			<div className='header__logo'>
				<Logo />
			</div>
			<div className='header__logout'>
				<p>Yevhen</p>
				<Button buttonText={BUTTON_TEXT_LOGOUT} />
			</div>
		</div>
	);
};

export default Header;
