import { useState } from 'react';
import uuid from 'react-uuid';

import { toCurrentDuration } from '../../helpers/pipeDuration';
import { formatDate } from '../../helpers/dateGenerator';
import {
	BUTTON_TEXT_ADD_AUTHOR,
	BUTTON_TEXT_DELETE_AUTHOR,
	BUTTON_TEXT_CREATE_COURSE,
	BUTTON_TEXT_CREATE_AUTHOR,
	INPUT_LABEL_TITLE,
	INPUT_LABEL_AUTHOR_NAME,
	INPUT_LABEL_DURATION,
} from '../../constants';
import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';

import './createCourses.scss';

const CreateCourse = ({
	authors,
	createNewAuthor,
	createNewCourse,
	toggleShow,
}) => {
	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const [localAuthors, setLocalAuthors] = useState(authors);
	const [newAuthor, setNewAuthor] = useState('');
	const [courseAuthors, setCourseAuthors] = useState([]);
	const [duration, setDuration] = useState('');

	const createAuthor = () => {
		if (newAuthor.length < 2) return;
		const authorObj = { id: uuid(), name: newAuthor };
		createNewAuthor(authorObj);
		setLocalAuthors([...localAuthors, authorObj]);
		setNewAuthor('');
	};

	const addCourseAuthor = (id) => {
		const addedAuthor = localAuthors.filter((item) => item.id === id)[0];
		setLocalAuthors(localAuthors.filter((item) => item.id !== id));
		setCourseAuthors([...courseAuthors, addedAuthor]);
	};

	const deleteCourseAuthor = (id) => {
		const deletedAuthor = courseAuthors.filter((item) => item.id === id)[0];
		setLocalAuthors([...localAuthors, deletedAuthor]);
		setCourseAuthors(courseAuthors.filter((item) => item.id !== id));
	};

	const validateForm = () => {
		return (
			title && description.length >= 2 && courseAuthors.length !== 0 && duration
		);
	};

	const resetForm = () => {
		setTitle('');
		setDescription('');
		setLocalAuthors(authors);
		setNewAuthor('');
		setCourseAuthors([]);
		setDuration('');
	};

	const addNewCourse = () => {
		if (!validateForm()) {
			alert('Please, fill in all fields');
			return;
		}
		toggleShow();
		createNewCourse({
			id: uuid(),
			title,
			description,
			creationDate: formatDate(new Date()),
			duration: +duration,
			authors: courseAuthors.map((item) => item.id),
		});
		resetForm();
	};

	const authorsList = localAuthors.map((item) => {
		return (
			<li key={item.id} className='authors__right-authorsList'>
				<p>{item.name}</p>
				<Button
					buttonText={BUTTON_TEXT_ADD_AUTHOR}
					onClick={() => addCourseAuthor(item.id)}
				/>
			</li>
		);
	});

	const courseAuthorsList = (courseAuthors) => {
		return courseAuthors.length === 0 ? (
			<p style={{ textAlign: 'center' }}>Author list is empty</p>
		) : (
			courseAuthors.map((item) => {
				return (
					<li key={item.id} className='authors__right-authorsList'>
						<p>{item.name}</p>
						<Button
							buttonText={BUTTON_TEXT_DELETE_AUTHOR}
							onClick={() => deleteCourseAuthor(item.id)}
						/>
					</li>
				);
			})
		);
	};

	return (
		<form onSubmit={(e) => e.preventDefault()} className='createCourses'>
			<div className='createCourses__top'>
				<div>
					<Input
						labelTag='enterTitle'
						value={title}
						labelText={INPUT_LABEL_TITLE}
						placeholderText='Enter title...'
						onChange={(e) => setTitle(e.target.value)}
					/>
				</div>
				<Button buttonText={BUTTON_TEXT_CREATE_COURSE} onClick={addNewCourse} />
			</div>
			<label className='createCourses__label' htmlFor='description'>
				Description
			</label>
			<textarea
				value={description}
				className='createCourses__textarea'
				name='Description'
				id='description'
				placeholder='Enter description'
				onChange={(e) => setDescription(e.target.value)}
			/>
			<div className='authors'>
				<div className='authors__left'>
					<h3 className='authors__left-author'>Add author</h3>
					<Input
						labelTag='enterAuthorName'
						value={newAuthor}
						onChange={(e) => setNewAuthor(e.target.value)}
						labelText={INPUT_LABEL_AUTHOR_NAME}
						placeholderText='Enter author name...'
					/>
					<Button
						buttonText={BUTTON_TEXT_CREATE_AUTHOR}
						onClick={createAuthor}
					/>
					<h3 className='authors__left-duration'>Duration</h3>
					<Input
						labelTag='enterDuration'
						type={'number'}
						value={duration}
						onChange={(e) => setDuration(e.target.value)}
						labelText={INPUT_LABEL_DURATION}
						placeholderText='Enter duration in minutes...'
					/>
					<p>Duration: {toCurrentDuration(duration)} </p>
				</div>
				<div className='authors__right'>
					<h3 className='authors__right-author'>Authors</h3>
					<ul>{authorsList}</ul>
					<h3 className='authors__right-author'>Course authors</h3>
					<ul>{courseAuthorsList(courseAuthors)}</ul>
				</div>
			</div>
		</form>
	);
};

export default CreateCourse;
