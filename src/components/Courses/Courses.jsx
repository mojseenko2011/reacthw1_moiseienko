import { useState, useEffect } from 'react';

import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';

import './courses.scss';

const Courses = ({ courses, authors, toggleShow }) => {
	const [localCourses, setLocalCourses] = useState(courses);
	const [searchCoursesInput, setSearchCoursesInput] = useState('');

	useEffect(() => {
		setLocalCourses(courses);
	}, [courses]);

	// This function checks our input, if it is empty, then sets courses to the
	// initial value. Otherwise writes input values to searchCoursesInput
	const changeSearchCoursesInput = (value) => {
		if (value.length < 1) {
			return setLocalCourses(courses);
		}
		setSearchCoursesInput(value);
	};

	//Filters courses by title, returns an array of courses in titles that contain
	// the entered text, and writes this array in courses
	const filterCourses = (text) => {
		if (text.length === 0) {
			return;
		}
		let filteredCourses = localCourses.filter(
			(item) =>
				item.title.toLowerCase().indexOf(text.toLowerCase()) > -1 ||
				item.id === text
		);
		setLocalCourses(filteredCourses);
	};

	const courseList = localCourses.map((item) => {
		const { id, ...props } = item;
		return (
			<li key={id}>
				<CourseCard props={props} authorsArr={authors} />
			</li>
		);
	});
	return (
		<div className='courses'>
			<SearchBar
				changeSearchCoursesInput={changeSearchCoursesInput}
				filterCourses={filterCourses}
				searchCoursesInput={searchCoursesInput}
				toggleShow={toggleShow}
			/>
			<ul>{courseList}</ul>
		</div>
	);
};

export default Courses;
