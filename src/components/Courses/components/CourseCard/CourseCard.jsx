import Button from '../../../../common/Button/Button';
import { BUTTON_TEXT_SHOW } from '../../../../constants';
import {
	toCurrentDuration,
	toAuthorsString,
} from '../../../../helpers/pipeDuration';

import './courseCard.scss';

const CourseCard = ({ props, authorsArr }) => {
	const { title, description, creationDate, duration, authors } = props;
	return (
		<div className='courseCard'>
			<div className='courseCard__description'>
				<h2>{title}</h2>
				<p>{description}</p>
			</div>
			<div className='courseCard__authors'>
				<p>
					<span>Authors: </span>
					{toAuthorsString(authors, authorsArr)}
				</p>
				<p>
					<span>Duration: </span>
					{toCurrentDuration(`${duration}`)}
				</p>
				<p>
					<span>Created: </span>
					{creationDate}
				</p>
				<div className='courseCard__button'>
					<Button buttonText={BUTTON_TEXT_SHOW} />
				</div>
			</div>
		</div>
	);
};

export default CourseCard;
