import { useState } from 'react';

import Input from '../../../../common/Input/Input';
import Button from '../../../../common/Button/Button';
import {
	BUTTON_TEXT_SEARCH,
	BUTTON_TEXT_ADD_COURSE,
} from '../../../../constants';

import './searchBar.scss';

const SearchBar = ({
	changeSearchCoursesInput,
	filterCourses,
	searchCoursesInput,
	toggleShow,
}) => {
	const [courseName, setCourseName] = useState('');

	const changeInput = (e) => {
		setCourseName(e.target.value);
		changeSearchCoursesInput(e.target.value);
	};

	return (
		<div className='container'>
			<div className='container__flex'>
				<Input
					value={courseName}
					onChange={(e) => changeInput(e)}
					placeholderText='Enter course name...'
				/>
				<Button
					onClick={() => filterCourses(searchCoursesInput)}
					buttonText={BUTTON_TEXT_SEARCH}
				/>
			</div>
			<div>
				<Button buttonText={BUTTON_TEXT_ADD_COURSE} onClick={toggleShow} />
			</div>
		</div>
	);
};

export default SearchBar;
