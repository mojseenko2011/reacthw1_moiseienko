import './input.scss';

const Input = (props) => {
	const { labelText, placeholderText, onChange, value, type, labelTag } = props;

	return (
		<>
			{labelTag ? (
				<label className='label' htmlFor={labelTag}>
					{labelText}
				</label>
			) : null}
			<input
				id={labelTag}
				type={type}
				className='input'
				value={value}
				placeholder={placeholderText}
				onChange={onChange}
			/>
		</>
	);
};

export default Input;
