import { useState } from 'react';
import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import CreateCourse from './components/CreateCourse/CreateCourse';
import { mockedAuthorsList, mockedCoursesList } from './constants';

import './App.scss';

const App = () => {
	const [authors, setAuthors] = useState(mockedAuthorsList);
	const [courses, setCourses] = useState(mockedCoursesList);
	const [isShow, setIsShow] = useState(true);

	const createNewAuthor = (value) => {
		setAuthors([...authors, value]);
	};

	const createNewCourse = (obj) => {
		setCourses([...courses, obj]);
	};

	const toggleShow = () => {
		setIsShow((isShow) => !isShow);
	};

	return (
		<div className='app'>
			<Header />
			{isShow ? (
				<Courses courses={courses} authors={authors} toggleShow={toggleShow} />
			) : (
				<CreateCourse
					authors={authors}
					createNewAuthor={createNewAuthor}
					createNewCourse={createNewCourse}
					toggleShow={toggleShow}
				/>
			)}
		</div>
	);
};

export default App;
